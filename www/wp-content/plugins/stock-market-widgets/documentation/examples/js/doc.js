$(document).ready(function() {
  var codeHeading = '<h3>Copy and paste the following HTML code into your application:</h3>';
  var codeAsString = $('body').data('code-as-string') ? $('body').data('code-as-string') : false;
  $('.sm-widget').each(function () {
    var $this = $(this);
    // table widget
    if ($this.prop("tagName")=='TR') {
      $this.closest('table').after(
        '<pre>' +
        '    <code></code>' +
        '</pre>'
      );
      return false;
    } else if ($this.prop("tagName")=='SPAN') {
      $this.closest('.sm-marquee').after(
        '<pre>' +
        '    <code></code>' +
        '</pre>'
      );
      return false;
    } else {
      $this.after(
        '<pre>' +
        '    <code></code>' +
        '</pre>'
      );
    }
  });

  $('pre').each(function() {
    var $this = $(this);
    var $code = $this.find('code');
    var $widget = $this.prev('.sm-widget, .sm-sortable-table, .sm-marquee');
    var plain_html = $widget.clone().wrap('<div>').parent().html();
    if (codeAsString) plain_html = plain_html.replace(/\n/g,"").replace(/\>\s+\</g,'><');
    var esc_html = $('<div/>').text(plain_html).html();
    $code.html(esc_html);
    hljs.highlightBlock(this);
    $this.prepend(codeHeading);
  });
});