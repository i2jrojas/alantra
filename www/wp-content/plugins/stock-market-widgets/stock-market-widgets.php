<?php
/**
 * Plugin Name: Stock Market Widgets (WordPress plugin)
 * Plugin URI: http://webthegap.com/products/stock-market-wordpress/
 * Description: This plugin allows to embed live stocks and currencies market data (as well as key statistics, charts and news headlines) using Yahoo Finance API.
 * Version: 1.0.9, built on Wed Jul 13 2016
 * Author: WebTheGap - Professional Web Development Services
 * Author URI: http://webthegap.com
 * Copyright WebTheGap <mail@webthegap.com>
 *
 */
if (!defined('ABSPATH')) die('Direct access denied');

add_action('wp_enqueue_scripts', 'load_assets');

add_action('widgets_init',
  create_function('', 'return register_widget("StockMarket_Widget");')
);

function load_assets() {
  wp_enqueue_style('sm-css', plugin_dir_url(__FILE__).'css/sm-style.css');
  wp_enqueue_script('sm-js', plugin_dir_url(__FILE__).'js/sm_widgets.min.js', array ('jquery'), '1.0.9', FALSE);
}


/**
* StockMarket_Widget widget.
*/
class StockMarket_Widget extends WP_Widget {
  /**
  * Register widget with WordPress.
  */
  function __construct() {
    parent::__construct(
      'SM_Widget', // Base ID
      __('Stock Market Widget'), // Name
      array('description' => __('Market data for a given stock symbol or currency pair')) // Args
    );
  } 

  /**
  * Back-end widget form.
  *
  * @see WP_Widget::form()
  *
  * @param array $instance Previously saved values from database.
  */
  public function form($instance) {
    $title = isset($instance['title']) ? $instance['title'] : __('Stock price for...');
    $widget_markup = isset($instance['widget_markup']) ? $instance['widget_markup'] : '';
    
    ?>
    <p>
      <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
      <input class="widefat" type="text" id="<?php echo $this->get_field_id('title');?>" name="<?php echo $this->get_field_name('title');?>" value="<?php echo esc_attr($title); ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id('widget_markup'); ?>"><?php _e('Widget HTML Markup:'); ?></label>
      <textarea rows="20" class="widefat" id="<?php echo $this->get_field_id('widget_markup'); ?>" name="<?php echo $this->get_field_name('widget_markup'); ?>"><?php echo esc_attr($widget_markup); ?></textarea>
    </p>
    <?php
  }
  /**
  * Sanitize widget form values as they are saved.
  *
  * @see WP_Widget::update()
  *
  * @param array $new_instance Values just sent to be saved.
  * @param array $old_instance Previously saved values from database.
  *
  * @return array Updated safe values to be saved.
  */
  public function update($new_instance, $old_instance) {
    $instance = array();
    $instance['title'] = !empty($new_instance['title']) ? strip_tags($new_instance['title']) : '';
    $instance['widget_markup'] = !empty($new_instance['widget_markup']) ? $new_instance['widget_markup'] : '';
    return $instance;
  }
  
  /**
   * Front-end display of widget.
   *
   * @see WP_Widget::widget()
   *
   * @param array $args Widget arguments.
   * @param array $instance Saved values from database.
   */
  public function widget($args, $instance) {
    echo $args['before_widget'];
    
    if (!empty( $instance['title'])) {
      echo $args['before_title'] . apply_filters('widget_title', $instance['title']). $args['after_title'];
    }
    
    if (!empty( $instance['widget_markup'])) {
      echo $instance['widget_markup'];
    }
    
    echo $args['after_widget'];
  }  
}